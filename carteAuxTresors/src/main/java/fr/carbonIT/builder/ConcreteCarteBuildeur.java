package fr.carbonIT.builder;

import java.util.ArrayList;
import java.util.List;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.cases.ICase;
import fr.carbonIT.model.cases.Montagne;
import fr.carbonIT.model.position.Orientation;
import fr.carbonIT.model.position.Position;
import fr.carbonIT.util.GestionnairePosition;

public class ConcreteCarteBuildeur implements ICarteBuildeur{
	
	private Carte carte;
	private List<Aventurier> aventuriers = new ArrayList<Aventurier>();
	
	@Override
	public void creerCarte(int nombreLignes, int nombreColones) {
		// TODO Auto-generated method stub
		this.carte = new Carte(nombreLignes, nombreColones);
	}

	@Override
	public void creerCase(int positionHorizontale,int positionVertical) {
		// TODO Auto-generated method stub
		Position position = new Position(positionHorizontale, positionVertical);
		ICase c = new Montagne(position);
		this.carte.modifierCase(c);
	}

	@Override
	public void placerTresor(int positionHorizontal, int positionVertical, int nombreTresor) {
		// TODO Auto-generated method stubs
		Position position = new Position(positionHorizontal, positionVertical);
		this.carte.getCase(position).setNombreTresor(nombreTresor);
	}

	@Override
	public void creerAventurier(String nom, int positionHorizontal, int positionVertical, Orientation orientation,
			String sequence) {
		// TODO Auto-generated method stub
		Position position = new Position(positionHorizontal, positionVertical);
		Aventurier aventurier = new Aventurier(nom, position, orientation, sequence, this.carte);
		Aventurier.setGestionnaireInstance(new GestionnairePosition(this.carte));
		this.carte.getCase(position).setAventurier(aventurier);
		this.aventuriers.add(aventurier);
	}
	
	@Override
	public Carte recupererCarte() {
		// TODO Auto-generated method stub
		return this.carte;
	}

	@Override
	public List<Aventurier> recupererAventeriers() {
		// TODO Auto-generated method stub
		return this.aventuriers;
	}
	
	

}
