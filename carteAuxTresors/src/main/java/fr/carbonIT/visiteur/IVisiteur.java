package fr.carbonIT.visiteur;


import java.util.List;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.cases.ICase;
/*
 * Patron de conception Visiteur
 * Cette interface definit trois maniere pour iterer sur la carte. 
 */
public interface IVisiteur {
	
	// Pour recuperer les cases montagnes dans la carte.
	public List<ICase> getMontagnes();
	
	// Pour recuperer les cases qui ont des tresors dans la carte.
	public List<ICase> getTresors();
	
	// Pour recuperer les aventuriers qui sont dans les cases de la carte.
	public List<Aventurier> getAventeriers();
	
}
