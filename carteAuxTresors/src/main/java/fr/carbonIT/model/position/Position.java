package fr.carbonIT.model.position;

public class Position {
	
	private int vertical;
	private int horizental;
	
	public Position(int horizental, int vertical) {
		// TODO Auto-generated constructor stub
		this.horizental = horizental;
		this.vertical = vertical;
	}

	public int getVertical() {
		return vertical;
	}

	public void setVertical(int vertical) {
		this.vertical = vertical;
	}

	public int getHorizental() {
		return horizental;
	}

	public void setHorizental(int horizental) {
		this.horizental = horizental;
	}
	
}
