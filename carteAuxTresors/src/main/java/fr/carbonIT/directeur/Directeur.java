package fr.carbonIT.directeur;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import fr.carbonIT.builder.ICarteBuildeur;
import fr.carbonIT.model.position.Orientation;

/*
 * Patron de conception Buildeur.
 * Cette classe joue le role du directeur, elle synchronise la construction du modele.
 * NB: si le format du fichier source change vers XML ou JSON, on introduit une methode approprié,
 * exemple: chargerLaCarteDepuisFichierXML() pour XML ou chargerLaCarteDepuisFichierJSON() pour JSON.
 */
public class Directeur {

	private ICarteBuildeur buildeur;
	private Scanner scanneur;


	public Directeur(ICarteBuildeur builder, InputStream source) throws FileNotFoundException {
		// TODO Auto-generated constructor stub
		this.buildeur = builder;
		this.scanneur = new Scanner(source);
	}
	
	public void chargerLaCarteDepuisFichierPlat() {
		while (this.scanneur.hasNext()) {
			String ligne = this.scanneur.nextLine();
			if (!ligne.startsWith("#")) {
				if(ligne.startsWith("C")) {
					String[] tab = ligne.split("-");
					int nombreColones = Integer.parseInt(tab[1].trim());
					int nombreLignes = Integer.parseInt(tab[2].trim());
					this.buildeur.creerCarte(nombreLignes, nombreColones);
				}else if(ligne.startsWith("M")) {
					String[] tab = ligne.split("-");
					int positionVertical = Integer.parseInt(tab[1].trim());
					int positionHorizontale = Integer.parseInt(tab[2].trim());
					this.buildeur.creerCase(positionHorizontale, positionVertical);
				}else if(ligne.startsWith("T")) {
					String[] tab = ligne.split("-");
					int positionVertical = Integer.parseInt(tab[1].trim());
					int positionHorizontal = Integer.parseInt(tab[2].trim());
					int nombreTresor = Integer.parseInt(tab[3].trim());
					this.buildeur.placerTresor(positionHorizontal, positionVertical, nombreTresor);
				}else if (ligne.startsWith("A")) {
					String[] tab = ligne.split("-");
					String nom = tab[1].trim();
					int positionVertical = Integer.parseInt(tab[2].trim());
					int positionHorizontal = Integer.parseInt(tab[3].trim());
					String orient = tab[4].trim();
					Orientation orientation = null;
					switch (orient) {
					case "S":
						orientation = Orientation.Sud;
						break;
					case "N":
						orientation = Orientation.Nord;
						break;
					case "E":
						orientation = Orientation.Est;
						break;
					case "O":
						orientation = Orientation.Ouest;
						break;
					}
					String sequence = tab[5].trim();
					this.buildeur.creerAventurier(nom, positionHorizontal, positionVertical, orientation, sequence);
				}
			}
		}
		this.scanneur.close();
	}
}
