package fr.carbonIT.model.cases;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.position.Position;

/*
 * pour généraliser le traitement des cases par un seule Type "ICase",
 * Cette interface Contient des methodes avec un mot clé "default". 
 */
public interface ICase {
	
	/*
	 * Cette methode est utilisé pour verifier si la case est valide pour un aventurier pour ce positionné ou non,
	 * pour que l'aventurier avance sinon il reste à sa place. NB: "implémenter par les deux sous type Plaine et Montagne".
	 */
	public boolean estValid();
	
	/*
	 * Cette methode est implementer seulement dans la classe "Plaine" puisqu'elle peut contenir des tresors
	 * , mais pas dans la classe "Montagne".
	 */  
	default public boolean EstCeQueContientTresor() { return false;	}
	
	default public void decrementerNombreTresor() {}
	
	default public int getNombreTresor() { return 0; }
	
	default public void setNombreTresor(int nombreTresor) {}
	
	/*
	 * Cette methode est implementer seulement dans la Classe "Plaine" puisqu'elle peut contenir un aventurier
	 * , mais pas dans la classe "Montagne".
	 */
	default public Aventurier getAventurier() { return null; }
	
	default public void setAventurier(Aventurier aventurier) { }
	
	//Cette methode sera utiliser pour indiquer qu'une case a un aventurier dedans ou non.
	default public void setValide(boolean valide) {};
	
	public Position getPosition();
	
	public String getNature();
	
	
	
}
